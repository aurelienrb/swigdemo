#pragma once

#include <vector>
#include <utility>
#include <cstdint>

#ifdef SWIG
%include "enums.swg"
%javaconst(1);
#endif

namespace Analyzer {
    enum class MouthZones8 : uint32_t {
        LoLeftExt8,
        LoLeftInt8,
        LoRightExt8,
        LoRightInt8,
        UpLeftExt8,
        UpLeftInt8,
        UpRightExt8,
        UpRightInt8
    };

    enum class MouthZones12 : uint32_t {
        LoIncisorExt12,
        LoIncisorInt12,
        LoMolarLeftExt12,
        LoMolarLeftInt12,
        LoMolarRightExt12,
        LoMolarRightInt12,
        UpIncisorExt12,
        UpIncisorInt12,
        UpMolarLeftExt12,
        UpMolarLeftInt12,
        UpMolarRightExt12,
        UpMolarRightInt12
    };

    enum class MouthZones16 : uint32_t {
        LoIncisorExt16,
        LoIncisorInt16,
        LoMolarLeftExt16,
        LoMolarLeftInt16,
        LoMolarLeftOcc16,
        LoMolarRightExt16,
        LoMolarRightInt16,
        LoMolarRightOcc16,
        UpIncisorExt16,
        UpIncisorInt16,
        UpMolarLeftExt16,
        UpMolarLeftInt16,
        UpMolarLeftOcc16,
        UpMolarRightExt16,
        UpMolarRightInt16,
        UpMolarRightOcc16
    };

    using ZonePass8 = std::pair<MouthZones8, int>;
    using ZonePass12 = std::pair<MouthZones12, int>;
    using ZonePass16 = std::pair<MouthZones16, int>;
}

#ifdef SWIG
%include "std_vector.i"
%include "std_pair.i"

%template(ZonePass8) std::pair<Analyzer::MouthZones8, int>;
%template(ZonePass12) std::pair<Analyzer::MouthZones12, int>;
%template(ZonePass16) std::pair<Analyzer::MouthZones16, int>;
#endif
