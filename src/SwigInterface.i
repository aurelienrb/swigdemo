%module analyzer

%{
    #include "Analyzer.h"
%}

%include "MouthZones.h"

%include "std_vector.i"

%template(VectorInt) std::vector<int>;
%template(BrushingZones8) std::vector<Analyzer::ZonePass8>;

namespace Analyzer {
    std::vector<int> analyzeBrushingZones8(const std::vector<ZonePass8> & input);
}
