#include "Analyzer.h"

#include <algorithm>

namespace Analyzer {
    std::vector<int> analyzeBrushingZones8(const std::vector<ZonePass8> & input) {
        std::vector<int> result;
        result.reserve(input.size());

        for (auto p : input) {
            result.push_back(p.second);
        }
        std::reverse(result.begin(), result.end());
        
        return result;
    }
}
