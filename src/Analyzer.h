#pragma once

#include "MouthZones.h"

#include <vector>

namespace Analyzer {
    std::vector<int> analyzeBrushingZones8(const std::vector<ZonePass8> & input);
}
