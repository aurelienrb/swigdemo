public class TestAnalyzer {
    static {
	    System.loadLibrary("analyzer");
    }
    
    public static void main(String argv[]) {
        var input8 = makeInputZones8();

        System.out.println("Calling C++ code with:");
        print(input8);

        var output = analyzer.analyzeBrushingZones8(input8);

        System.out.println("Result from C++ code:");
        print(output);
    }

    private static BrushingZones8 makeInputZones8() {
        var result = new BrushingZones8();

        result.add(new ZonePass8(MouthZones8.LoLeftExt8, 8));
        result.add(new ZonePass8(MouthZones8.LoLeftInt8, 6));
        result.add(new ZonePass8(MouthZones8.UpRightInt8, 7));
        result.add(new ZonePass8(MouthZones8.UpLeftInt8, 3));

        return result;
    }

    private static void print(BrushingZones8 data) {
        for (int i = 0; i < data.size(); i++) {
            ZonePass8 p = data.get(i);
            System.out.println("- " + p.getFirst() + " : " + p.getSecond());
        }
    }

    private static void print(VectorInt data) {
        for (int i = 0; i < data.size(); i++) {
            int n = data.get(i);
            System.out.println("- " + n);
        }
    }
}
