#!/bin/bash

set -e

ROOT=`dirname "$0"`
SRCDIR="$ROOT/src"
GENDIR=$ROOT/generated

CPPFLAGS="-std=c++14 -Wall -Wextra -pedantic -Werror -fPIC"
JNI_INC="-I/usr/lib/jvm/java-11-openjdk-amd64/include -I/usr/lib/jvm/java-11-openjdk-amd64/include/linux"

echo "Build C++ kernel (static lib)"
g++ $CPPFLAGS -c -o Analyzer.o $SRCDIR/Analyzer.cpp

echo "Generate SWIG wrappers"
mkdir -p $GENDIR
rm -f $GENDIR/*
swig -c++ -java -outdir $GENDIR $SRCDIR/SwigInterface.i

echo "Build JAVA wrapper (JNI)"
javac $GENDIR/*.java

echo "Build C++ wrapper (JNI)"
g++ $CPPFLAGS $JNI_INC -shared -o libanalyzer.so $SRCDIR/*_wrap.cxx Analyzer.o

echo "Build JAVA example"
javac -classpath $GENDIR $ROOT/TestAnalyzer.java

echo "Run JAVA example"
export LD_LIBRARY_PATH=$PWD
export CLASSPATH="$ROOT:$GENDIR"
java TestAnalyzer
