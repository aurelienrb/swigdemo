# swigdemo

[![pipeline status](https://gitlab.com/aurelienrb/swigdemo/badges/master/pipeline.svg)](https://gitlab.com/aurelienrb/swigdemo/commits/master)

*Proof of concept* de l'utilisation de [swig](http://www.swig.org/) pour rendre un code C++14 utilisable depuis Java 11. L'ensemble est compilé et testé sous GitLab CI (donc sous arm64).

A noter que le compilateur java émet un warning relatif à la façon dont SWIG encapsule le type `std::vector`:

```
Note: Some input files use or override a deprecated API.
Note: Recompile with -Xlint:deprecation for details.
```
